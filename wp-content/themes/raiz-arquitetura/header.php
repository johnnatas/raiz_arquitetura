<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <?php include 'inc/meta-tags.php' ?>
    <link rel="stylesheet" href="<?= themedir() ?>/css/main.css">
    <title>Raiz Arquitetura</title>
    <?php wp_head() ?>
</head>
<body  <?php body_class(); ?>>
<div id="site-top"></div>
    <header>
        <div class="container">
            <div class="left">
                <div class="logo">
                    <a href="<?= site_url('/') ?>"><img src="<?php the_field('logo', 'options') ?>" alt="logo" title=""></a>
                </div>
            </div>
            <div class="right">
                <div class="menu-mobile">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <?php
                    wp_nav_menu([
                        "container" => "nav",
                        "container_class" => "nav menu"
                    ]);
                ?>
            </div>
        </div> <!-- fim container -->
    </header>