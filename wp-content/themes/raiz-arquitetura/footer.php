    
    <footer>
        <div class="container">
            <div class="col-xs-12 col-sm-4">
                <div class="logo-footer">
                    <img src="<?php the_field('logo_footer', 'options') ?>" alt="logo" title="Logo">
                </div>
                <div class="copyright">
                    <p>&copy Raiz Arquitetura</p>
                    <p>Todos os direitos reservados</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="menu-footer">
                    <?php 
                        wp_nav_menu();
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="redes-sociais">
                    <?php include 'inc/redes-sociais.php' ?>
                </div>
                <div class="info-footer">
                    <p><?php the_field('infos', 'options') ?></p>
                </div>
            </div>
        </div>
    </footer>
    <a href="#site-top" class="ancora"><i class="fas fa-chevron-up"></i></a>
    <div class="layer"></div>
    <script src="<?= themedir() ?>/js/plugins.min.js"></script>
    <script src="<?= themedir() ?>/js/app.js"></script>

    <?php if(is_page_template() == 'page-contato.php'): ?>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyABvANRAvi99WdL8XfZl8QdyWpRrvBcLvU"></script>
        <script type="text/javascript"  src="<?php themedir() ?>/js/map.js"></script>
    <?php endif ?>

    <?php wp_footer(); ?>
</body>
</html>