<?php 
    get_header();
    $fields = get_fields();
?>
    
<section class="banner-principal" style="background: url(<?= $fields["banner"] ?>) center/cover no-repeat">
</section>

<div class="single-projeto">
    <section>
        <div class="container">
            <div class="breadcrumb">
                <ul>
                    <li><a href="<?= site_url('/') ?>">Home</a></li>
                    <li><a href="<?= site_url('/projetos') ?>">Projetos</a></li>
                    <li><?php the_title() ?></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="content">
                        <h1><?php the_title() ?></h1>
                        <h2><?= $fields["titulo"] ?></h2>
                        <p>
                            <?= $fields["descricao"] ?>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="video">
                        <?php if(!empty($fields["video"])): ?>
                                <?= $fields["video"] ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="galeria-single">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php if(!empty($fields["galeria"])): ?>
                            <?php foreach( $fields["galeria"] as $img ): ?>
                                <div class="swiper-slide"><img src="<?php cropImage( $img["img_projeto"], 'img-galeria-swiper' )?>" alt="<?php the_title() ?>"></div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
</div>
<section class="center">
    <div class="container">
        <h1>Outros Projetos</h1>
    </div>
    <?php include 'inc/galeria.php' ?>
</section>
<section>
    <div class="container">
        <a href="<?= site_url("projetos") ?>"><button class="btn-default"><i class="fas fa-plus"></i></button></a>
    </div>
</section>
<?php get_footer() ?>