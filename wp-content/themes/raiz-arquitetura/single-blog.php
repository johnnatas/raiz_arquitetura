<?php
    get_header();
    $fields = get_fields();
?>
<div class="single-blog">
    <section class="banner-principal" style="background: url(<?= $fields["banner_principal"] ?>) center/cover no-repeat">
    </section>
    <div class="container">
        <section>
            <div class="col-xs-12 col-sm-8">
                <div class="content">
                    <h1><?php the_title() ?></h1>
                    <h5><?= the_time('d/m/Y') ?></h5>
                    <a href="http://facebook.com/share.php?u=http:<?php the_permalink() ?>" target="_blank"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://twitter.com/intent/tweet?url=<?php the_permalink() ?>&text=<?php the_title() ?>" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a href="https://pinterest.com/pin/create/bookmarklet/?media=<?php the_post_thumbnail_url() ?>&url=<?php the_permalink() ?>&description=<?php the_title() ?>" target="_blank"><i class="fab fa-pinterest-square"></i></a>
                </div>
                <div class="descricao">
                    <p>
                        <?= $fields["descricao"] ?>
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="sidebar">
                    <?php get_sidebar() ?>
                </div>
            </div>
        </section>
    </div>
</div>

    <section class="banner-divisor" style="background: url(<?= $fields["banner_secundario"] ?>) center/cover no-repeat">
        <div class="links-redes-sociais">
            <?php include 'inc/redes-sociais.php' ?>
        </div>
    </section>

<section class="center">
    <div class="container">
        <h1>Outros Projetos</h1>
    </div>
    <?php include 'inc/galeria.php' ?>
</section>
<section>
    <div class="container">
        <a href="<?= site_url("projetos") ?>"><button class="btn-default"><i class="fas fa-plus"></i></button></a>
    </div>
</section>
<?php get_footer(); ?>