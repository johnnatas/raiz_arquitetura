<?php
// Template name: Sobre

get_header();

$fields = get_fields();
?>
<div class="sobre">
    <section class="banner-principal" style="background: url(<?= $fields["banner_principal"] ?>) center/cover no-repeat">
    </section>
    <section class="bloco1-sobre">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h1><?= $fields["ttl_sobre"] ?></h1>
                    <p><?= $fields["texto_sobre"] ?></p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="video">
                        <?= $fields["video_sobre"] ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <p><?= $fields["missao"] ?></p>            
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p><?= $fields["visao-sobre"] ?></p>            
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p><?= $fields["valores"] ?></p>                
                </div>
            </div>
        </div>
    </section>
    <section class="banner-divisor" style="background: url(<?= $fields["banner-secao"] ?>) center/cover no-repeat">
        <div class="links-redes-sociais">
            <?php include 'inc/redes-sociais.php' ?>
        </div>
    </section>
    <section>
        <div class="container">
            <?php foreach($fields["integrantes"] as $membro): ?>
                <div class="col-xs-12 col-sm-4 box">
                    <figure>
                        <img src="<?php cropImage( $membro["img"], 'img_thumb' ) ?>" alt="integrante" class="img-responsive">
                    <figure>
                    <h2><?= $membro["nome"] ?></h2>
                    <h5><?= $membro["cargo"] ?></h5>
                    <a href="mailto:<?= $membro["email"] ?>" title="Enviar e-mail"><i class="fas fa-at"></i></a>
                </div>
            <?php endforeach ?>
        </div>
    </section>
    <section class="center">
        <div class="depoimentos">
            <h1><?= get_field("depoimentos_title", 8) ?></h1>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php if(!empty( get_field("depoimentos", 8) )): ?>
                        <?php foreach( get_field("depoimentos", 8) as $item ): ?>
                            <div class="swiper-slide">
                                <p><?= $item["depoimento"] ?></p>
                                <p><?= $item["autor"] ?></p>
                                <p><?= $item["nome_projeto"] ?></p>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>