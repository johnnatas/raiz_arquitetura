<?php 
    $posts = new WP_Query([
        "post_type" => "blog"
    ]);
?>
<div class="sidebar">
    <ul>
        <?php if($posts->have_posts()): ?>
            <?php $cont = 0 ?>
            <?php while($posts->have_posts() && $cont < 6): $posts->the_post(); ?>
                <li>
                    <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                    <p><?php the_time('d/m/Y') ?></p>
                </li>
            <?php $cont++ ?>
            <?php endwhile ?>
        <?php endif ?>
    </ul>
</div>