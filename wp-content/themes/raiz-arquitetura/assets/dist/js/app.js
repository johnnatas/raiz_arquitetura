(function (App, $) {

		App.events = App.events || {
			_init: function () {
				$(function () {
					App.events._click();
					App.events._load();
					App.events._scroll();
					App.events._resize();
					App.events._mobile();
				});
			},

			_mobile: function () {
			},

			_click: function () {
                var clicado;

                $('.menu-mobile').click( function (){
                    if(clicado){
                        $('.menu > ul').css('right', '0');
						$('.menu-mobile').addClass('open-menu');
						$('.layer').addClass('active');

                        clicado = false;
                    } else {
                        $('.menu > ul').css('right', '-100%');
						$('.menu-mobile').removeClass('open-menu');
						$('.layer').removeClass('active');
                        
                        clicado = true;
                    }
				});
				
				$('.layer').click(function(){
					$('.menu > ul').css('right', '-100%');
					$('.menu-mobile').removeClass('open-menu');
					$('.layer').removeClass('active');
					
					clicado = true;
				});

				$('a.ancora[href^="#"]').on('click', function(e) {
					e.preventDefault();
					var id = $(this).attr('href'),
					targetOffset = $(id).offset().top;
					  
					$('html, body').animate({ 
					  scrollTop: targetOffset - 100
					}, 900);
				
				});
			},

			_load: function () {
				var swiper = new Swiper('.swiper-container', {
					spaceBetween: 30,
					centeredSlides: true,
					autoplay: {
					  delay: 2500,
					  disableOnInteraction: false,
					},
					pagination: {
					  el: '.swiper-pagination',
					  clickable: true,
					}
				});
				
				$(window).load(function(){
					var tela = $(window).width();
					var tamanho = 200;
					
					if( tela >= 1510 ){
						tamanho = 300;
					}

					$('.galeria').BalancedGallery({
						autoresize: true,
						idealHeight: tamanho
					});
				});
			},

			_scroll: function () {
				$(window).on("scroll", function() {
					if($(window).scrollTop() > 50) {
					  $(".logo").addClass("scroll");
					  $(".menu-mobile").addClass("scroll");
					} else {
					  $(".logo").removeClass("scroll");
					  $(".menu-mobile").removeClass("scroll");
					}
				});
			},

			_resize: function () {     
			}
		};

		App.events._init();

})(window.App = window.App || {}, jQuery);