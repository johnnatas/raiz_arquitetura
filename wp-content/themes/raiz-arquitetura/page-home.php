<?php
//Template name: Home

get_header();

$fields = get_fields();
?>

<div class="banner-slide">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach( $fields["banner_slide"] as $item ): ?>
                <div class="swiper-slide">
                    <div style="background: url(<?= $item["img"] ?>) center/cover no-repeat" class="banner"></div>
                </div>
            <?php endforeach ?>
        </div>
        <!-- Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>


<section class="bloco1">
    <div class="container">
        <div class="col-sm-6">
            <h1><?= $fields["bloco1_title"] ?></h1>
            <p><?= $fields["texto_bloco1"] ?></p>
            <a href="<?= $fields["btn_bloco1"]["link"] ?>"><button class="btn-default"><?= $fields["btn_bloco1"]["texto_botao"] ?></button></a>
        </div>
        <div class="col-sm-6"></div>
    </div>
</section>

<section>
    <?php include 'inc/galeria.php' ?>
    
    <div class="container">
        <a href="<?= $fields["btn_bloco2"]["link"] ?>"><button class="btn-default"><?= $fields["btn_bloco2"]["texto_botao"] ?></button></a>
    </div>
</section>

<section class="banner-divisor" style="background: url(<?= $fields["banner_bloco3"] ?>) center/cover no-repeat">
    <div class="links-redes-sociais">
        <?php include 'inc/redes-sociais.php' ?>
    </div>
</section>

<section class="blog">
    <div class="container">
        <h1><?= $fields["bloco4_title"] ?></h1>
        <?php 
            $blog = new WP_Query([
                "post_type" => "blog"
            ]);
        ?>
        <?php if( $blog->have_posts() ): ?>
            <?php $cont = 0; ?>
            <?php while($blog->have_posts() && $cont < 3): $blog->the_post(); ?>
                <div class="col-xs-12 col-sm-4 box">
                    <a href="<?php the_permalink() ?>">
                        <figure>
                            <?php the_post_thumbnail() ?>
                        </figure>
                    </a>
                    <a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
                    <h5><?php the_time( 'd/m/Y' ); ?></h5>
                    <p><?= get_field('resumo', get_the_ID()) ?></p>
                </div>
            <?php $cont++; ?>
            <?php endwhile ?>
        <?php endif ?>
    <a href="<?= $fields["btn_bloco4"]["link"] ?>"><button class="btn-default"><?= $fields["btn_bloco4"]["texto_botao"] ?></button></a>
    </div>
</section>

<section class="center">
    <div class="depoimentos">
        <h1><?= $fields["depoimentos_title"] ?></h1>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php if(!empty($fields["depoimentos"])): ?>
                    <?php foreach( $fields["depoimentos"] as $item ): ?>
                        <div class="swiper-slide">
                            <p><?= $item["depoimento"] ?></p>
                            <p><?= $item["autor"] ?></p>
                            <p><?= $item["nome_projeto"] ?></p>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>

<?php get_footer() ?>