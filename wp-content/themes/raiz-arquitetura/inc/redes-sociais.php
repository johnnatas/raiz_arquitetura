<?php if( !empty(get_field('facebook', 'options')) ): ?>
    <div class="facebook-link"><a href="<?php the_field('facebook', 'options') ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
<?php endif ?>
<?php if( !empty(get_field('instagram', 'options')) ): ?>
    <div class="instagram-link"><a href="<?php the_field('instagram', 'options') ?>" target="_blank"><i class="fab fa-instagram"></i></a></div>
<?php endif ?>
<?php if( !empty(get_field('pinterest', 'options')) ): ?>
    <div class="pinterest-link"><a href="<?php the_field('pinterest', 'options') ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></div>
<?php endif ?>