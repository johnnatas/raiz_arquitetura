<meta charset="<?php bloginfo('charset'); ?>">
<meta description="Raiz Arquitetura">
<meta author="Jonatas Lopes">
<meta property="og:title" content="Raiz Arquitetura">
<meta property="og:url" content="<?= site_url('/') ?>">
<meta property="og:description" content="Raiz Arquitetura">
<meta name="theme-color" content="#000">
<meta name="apple-mobile-web-app-status-bar-style" content="#000">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="<?php cropImage( get_field('favicon', 'options'), 'favicon' ) ?>">

<!-- Safari -->
<link rel="apple-touch-icon" href="<?php cropImage( get_field('favicon', 'options'), 'favicon' ) ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php cropImage( get_field('favicon', 'options'), 'favicon' ) ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php cropImage( get_field('favicon', 'options'), 'favicon' ) ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php cropImage( get_field('favicon', 'options'), 'favicon' ) ?>">