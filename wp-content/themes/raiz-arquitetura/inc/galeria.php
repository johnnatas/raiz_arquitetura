<?php
    $projetos = new WP_Query([
        "post_type" => "projeto",
        "order" => "desc"
    ]);

    $limit = 0;
?>
<div class="galeria">
    <?php if($projetos->have_posts()): ?>
            <?php $count = 0 ?>
            <?php while( $projetos->have_posts() && $limit < 8 ): $projetos->the_post(); ?>
                <figure>
                    <a href="<?php the_permalink() ?>">
                        <img src="<?= get_field('img_destaque', get_the_ID()) ?>" alt="<?php the_title() ?>">
                        <?php $limit++; ?>
                        <figcaption>
                            <h3><?= the_title() ?></h3>
                        </figcaption>
                    </a>
                </figure>
            <?php endwhile ?>
    <?php endif ?>
</div>