<?php 
get_header();
//Template name: Projetos
?>
<section class="projetos">
    <div class="galeria">
        <?php
            $projetos = new WP_Query([
                "post_type" => "projeto",
                "posts_per_page" => 16,
                "paged" => get_query_var( 'paged' )
            ]);
        ?>
        <?php if($projetos->have_posts()): ?>
                <?php while( $projetos->have_posts() && $limit < 8 ): $projetos->the_post(); ?>
                    <figure>
                        <a href="<?php the_permalink() ?>"><img src="<?= get_field('img_destaque', get_the_id()) ?>" alt="<?php the_title() ?>">
                            <figcaption>
                                <h3><?= the_title() ?></h3>
                            </figcaption>
                        </a>
                    </figure>
                <?php endwhile ?>
        <?php endif ?>
    </div>
</section>
<section>
    <div class="container">
        <ul class="pagination-section">
            <?php 
                wp_pagenavi( array("query" => $projetos) );
            ?>
        </ul>
        <?php wp_reset_postdata(); ?>
    </div>
</section>
<?php get_footer(); ?>