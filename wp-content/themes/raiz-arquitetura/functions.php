<?php
add_action('after_setup_theme','add_custom_sizes');

function add_custom_sizes() {
  add_image_size('img-thumb', 350, 200, array('center','top'));
  add_image_size('img-galeria-swiper', 456, 258, array('center','top'));
  add_image_size('favicon', 70, 70, array('center','top'));
}

function cropImage($image_id,$crop_type) {
    $img = wp_get_attachment_image_src($image_id,$crop_type);
    echo $img[0];
}

// Remover opções menu para usuario comum
// function custom_menu_page_removing() {
    //remove_menu_page('edit.php');
//     remove_menu_page('upload.php');
//     remove_menu_page('edit-comments.php');
//     remove_menu_page('themes.php');
//     remove_menu_page('plugins.php');
//     remove_menu_page('tools.php');
//     remove_menu_page('options-general.php');
//     remove_menu_page('users.php');
//     remove_menu_page('edit.php?post_type=acf-field-group');
//     remove_menu_page('wpcf7'); 
// }
// if(get_current_user_id() > 1){
//     add_action( 'admin_menu', 'custom_menu_page_removing' );
// }

/* Key Google maps */
function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyABvANRAvi99WdL8XfZl8QdyWpRrvBcLvU');
}
add_action('acf/init', 'my_acf_init');

// Adiciona opções gerais no painel
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'     => 'Opções do tema',
        'menu_title'    => 'Opções do tema',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
}

function themedir() {
    echo bloginfo('template_url').'/assets/dist';
}

function themeSetup() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'themeSetup');

register_nav_menus( array(
    "main-menu"  =>  __("Menu Principal", "raiz-arquitetura"),
));