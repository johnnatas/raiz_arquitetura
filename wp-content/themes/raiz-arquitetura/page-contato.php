<?php 
// Template name: Contato

get_header();

$fields = get_fields();
?>

<section class="banner-principal" style="background: url( <?= $fields["banner"] ?> ) center/cover no-repeat"></section>

<section class="contato bloco1">
    <div class="container">
        <h1>Contato</h1>
        <div class="links-redes-sociais">
            <?php include 'inc/redes-sociais.php' ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form">
                <?= do_shortcode( '[contact-form-7 id="5" title="Contato"]' ) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="info-contato">
                <?= $fields["info_contato"] ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="contact-map" data-lat="<?php echo $fields['mapa']['lat'] ?>" data-lng="<?php echo $fields['mapa']['lng']; ?>" data-title="<?php echo bloginfo('name'); ?>" data-address="<?php echo $fields['mapa']['address']; ?>"></div>
</section>

<?php get_footer(); ?>