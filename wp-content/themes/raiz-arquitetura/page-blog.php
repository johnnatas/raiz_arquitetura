<?php 
get_header();
//Template name: Blog

$blog = new WP_Query([
    "post_type" => "blog",
    "posts_per_page" => 9,
    "paged" => get_query_var( 'paged' )
]);
?>
<section class="banner-principal" style="background: url(<?= get_field("banner_principal") ?>) center/cover no-repeat"></section>

<section>
    <div class="container">
        <?php if( $blog->have_posts() ): ?>
            <?php $i = 0 ?>
                <?php while($blog->have_posts()): $blog->the_post(); ?>
                    <?php 
                        $i+=1;
                        if($i%3 == 1 && $i%3 != 0){
                            echo '<div class="row">';
                        }
                    ?>
                        <div class="col-xs-12 col-sm-4 box">
                            <a href="<?php the_permalink() ?>">
                                <figure>
                                    <?php the_post_thumbnail() ?>
                                </figure>
                            </a>
                            <a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
                            <h5><?php the_time( 'd/m/Y' ); ?></h5>
                            <p><?= get_field('resumo', get_the_ID()) ?></p>
                        </div>
                    <?php
                        if( $i % 3 < 1){
                            echo '</div>';
                        }
                    ?>
                <?php endwhile ?>
        <?php endif ?>
        <ul class="pagination-section">
            <?php 
                wp_pagenavi( array("query" => $blog) );
            ?>
        </ul>
    </div>
</section>
<?php wp_reset_postdata(); ?>

<section class="center">
    <div class="container">
        <h1>Outros Projetos</h1>
    </div>
    <?php include 'inc/galeria.php' ?>
</section>
<section>
    <div class="container">
        <a href="<?= site_url("projetos") ?>"><button class="btn-default"><i class="fas fa-plus"></i></button></a>
    </div>
</section>
<?php get_footer(); ?>