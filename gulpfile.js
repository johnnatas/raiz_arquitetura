var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var minifycss = require('gulp-clean-css');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

var theme_dir = './wp-content/themes/raiz-arquitetura';

var files = [
    theme_dir+'/assets/dev/vendor/css/*.css',
    theme_dir+'/assets/dev/vendor/js/*.js',
    theme_dir+'/assets/dev/sass/**/*.scss',
    theme_dir+'/assets/dev/js/*.js',
    theme_dir+'/**/*.php'
];

gulp.task('browser-sync', () => {
    browserSync.init(files, {
        proxy: "localhost/raiz-arquitetura",
        notify: false
    });
});

gulp.task('images', () => {
    gulp.src(theme_dir+'/assets/dev/img/**/*')
      .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    	.pipe(gulp.dest(theme_dir+'/assets/dist/img/'));
});


gulp.task('sass', () => {
  gulp.src([
    theme_dir+'/assets/dev/vendor/css/*.css',
    theme_dir+'/assets/dev/sass/main.scss'
  ])
  .pipe(plumber({ 
    errorHandler: function (error) {
      console.log(error.message);
      this.emit('end');
    }
  }))
  .pipe(sass())
  .pipe(concatCss('main.css'))
  .pipe(cssmin())
  .pipe(gulp.dest(theme_dir+'/assets/dist/css'))
  .pipe(browserSync.reload({ stream: true }));
});

gulp.task('plugins-js', () => {
	return gulp.src([ 
    theme_dir+'/assets/dev/vendor/js/jquery.min.js',
    theme_dir+'/assets/dev/vendor/js/bootstrap.js',
  	theme_dir+'/assets/dev/vendor/js/*.js'
  ])
  .pipe(plumber({
    errorHandler: function (error) {
      console.log(error.message);
      this.emit('end');
    }
  }))
  .pipe(concat('plugins.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest(theme_dir+'/assets/dist/js'))
  .pipe(browserSync.reload({ stream: true }));
});

gulp.task('app', () => {
  return gulp.src(theme_dir+'/assets/dev/js/*.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    // .pipe(uglify())
    .pipe(gulp.dest(theme_dir+'/assets/dist/js'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('bs-reload', function () {
  browserSync.reload();
});


gulp.task('default', ['browser-sync'], () => {
  gulp.watch([
    theme_dir+'/assets/dev/sass/**/*.scss', 
    theme_dir+'/assets/dev/vendor/css/*.css'
  ], ['sass', 'images']);
  gulp.watch(theme_dir+'/assets/dev/js/*.js', ['app','plugins-js']);
  gulp.watch(files, ['bs-reload']);
});